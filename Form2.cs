﻿using Google.Apis.Auth;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            
        }

        private  void button1_Click(object sender, EventArgs e)
        {
            try
            {
                
                string jwt = textBox1.Text;
                var set = new GoogleJsonWebSignature.ValidationSettings();
                List<string> list = new List<string>();
                list.Add("490656911534-qqb3p7a7lfpj3bif8rkj6vruvin7jqvl.apps.googleusercontent.com");
                set.Audience = list;
                
                var payload = GoogleJsonWebSignature.ValidateAsync(jwt, set).Result;

                // 异步调用
                //Task<GoogleJsonWebSignature.Payload> t = GetPayload(jwt, set);
                
                //var t2 = Task.Run(() => { return GoogleJsonWebSignature.ValidateAsync(jwt, set); });
               
                // 干别的事

                //var payload = t.Result;
                if (payload != null && !string.IsNullOrEmpty(payload.Email))
                {
                    MessageBox.Show(payload.Email + ":" + payload.Subject + ":" + payload.Name + ":" + payload.Picture);
                }
                else
                {
                    MessageBox.Show("validate error!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// 异步函数
        /// </summary>
        /// <param name="jwt"></param>
        /// <param name="set"></param>
        /// <returns></returns>
        private async Task<GoogleJsonWebSignature.Payload> GetPayload(string jwt, GoogleJsonWebSignature.ValidationSettings set)
        {
            var ret = await GoogleJsonWebSignature.ValidateAsync(jwt, set);
            // 234
            return ret;
        }

    }
}
