﻿using Newtonsoft.Json;
using OAuth;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApplication2
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 转入用户承认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Tools.Twitter.Twitter tw = new Tools.Twitter.Twitter("fMvyi3CkipvRI9vnkpDaAwYRY", "8WjG5EzzYfTRinAy0RY10EG2V5CKxScTpRzOrhlY724WhbiPBz");
            string token;
            string err;
            bool ok = tw.GetRequestToken(out token, out err);
            textBox1.Text = token;

            return;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //Fill These vars with your netsuite information **********************************************************************
                String urlString = "https://twitter.com/oauth/request_token";              //RESTlet uri
                String ckey = "fMvyi3CkipvRI9vnkpDaAwYRY";                   //Consumer Key
                String csecret = "8WjG5EzzYfTRinAy0RY10EG2V5CKxScTpRzOrhlY724WhbiPBz";                //Consumer Secret
                String tkey = "";                   //Token ID
                String tsecret = "";                //Token Secret
                String netsuiteAccount = "";        //Netsuite Account to connect to (i.e 5624525_SB1)
                //**********************************************************************************************************************

                Uri url = new Uri(urlString);
                OAuthBase req = new OAuthBase();
                String timestamp = req.GenerateTimeStamp();
                //String timestamp = "1659426938";
                String nonce = req.GenerateNonce();
                //String nonce= "2342424234";
                String norm = "";
                String norm1 = "";
                String signature = req.GenerateSignature(url, ckey, csecret, tkey, tsecret, "GET", timestamp, nonce, out norm, out norm1);
                

                //Percent Encode (Hex Escape) plus character
                //if (signature.Contains("+"))
                //{
                //    signature = signature.Replace("+", "%2B");
                //}

                String header = "Authorization: OAuth ";
                //header += "oauth_callback=\"" + System.Web.HttpUtility.UrlEncode("https://www.pcbgogo2.com/member/login") + "\",";
                header += "oauth_consumer_key=" + ckey + ",";
                header += "oauth_nonce=" + nonce + ",";
                header += "oauth_signature_method=HMAC-SHA1,";
                header += "oauth_timestamp=" + timestamp + ",";
                header += "oauth_version=1.0,";
                header += "oauth_signature=" + signature;
                
                //header += "oauth_token=\"" + tkey + "\",";
                //header += "realm=\"" + netsuiteAccount + "\"";

                Console.Write(header);

                HttpWebRequest request =
               (HttpWebRequest)WebRequest.Create(urlString);

                //request.ContentType = "application/json";
                request.Method = "GET";
                request.Headers.Add(header);
                WebResponse response = request.GetResponse();
                HttpWebResponse httpResponse = (HttpWebResponse)response;

                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    //Console.WriteLine(JsonConvert.DeserializeObject(reader.ReadToEnd()));
                    string aa= reader.ReadToEnd();
                    //dynamic obj = JsonConvert.DeserializeObject(aa);
                    //MessageBox.Show(aa);
                    textBox1.Text = aa;
                }

            }
            catch (System.Net.WebException ex)
            {
                HttpWebResponse response = ex.Response as HttpWebResponse;
                switch ((int)response.StatusCode)
                {
                    case 401:
                        Console.WriteLine("Unauthorized, please check your credentials");
                        break;
                    case 403:
                        Console.WriteLine("Forbidden, please check your credentials");
                        break;
                    case 404:
                        Console.WriteLine("Invalid Url");
                        break;
                }
                Console.WriteLine("Code: " + (int)response.StatusCode);

                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    Console.WriteLine("Response: " + JsonConvert.DeserializeObject(reader.ReadToEnd()));

                }

            }
            catch (System.Exception ex1)
            {
                Console.WriteLine(ex1.ToString());
            }






        }

        /// <summary>
        /// 取得用户信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                String link = textBox4.Text;
                String[] token = this.GetTokenFromLink(link);

                //Fill These vars with your netsuite information **********************************************************************
                String urlString = "https://api.twitter.com/oauth/access_token?";              //RESTlet uri
                String ckey = "fMvyi3CkipvRI9vnkpDaAwYRY";                   //Consumer Key
                String csecret = "8WjG5EzzYfTRinAy0RY10EG2V5CKxScTpRzOrhlY724WhbiPBz";                //Consumer Secret
                String tkey = token == null ? "" : token[0] ;                   //Token ID
                textBox3.Text = tkey;
                String tsecret = "";                //Token Secret
                //String netsuiteAccount = "";        //Netsuite Account to connect to (i.e 5624525_SB1)
                String verifier = token == null ? "" : token[1];
                textBox2.Text = verifier;
                //**********************************************************************************************************************

                Uri url = new Uri(urlString);
                OAuthBase req = new OAuthBase();
                String timestamp = req.GenerateTimeStamp();
                //String timestamp = "1659426938";
                String nonce = req.GenerateNonce();
                //String nonce= "2342424234";
                String norm = "";
                String norm1 = "";
                String signature = req.GenerateSignature(url, ckey, csecret, tkey, tsecret, verifier, "GET", timestamp, nonce, out norm, out norm1);


                String header = "Authorization: OAuth ";
                //header += "oauth_callback=\"" + System.Web.HttpUtility.UrlEncode("https://www.pcbgogo2.com/member/login") + "\",";
                header += "oauth_consumer_key=" + ckey + ",";
                header += "oauth_nonce=" + nonce + ",";
                header += "oauth_signature_method=HMAC-SHA1,";
                header += "oauth_timestamp=" + timestamp + ",";
                header += "oauth_version=1.0,";
                header += "oauth_signature=" + signature + ",";
                header += "oauth_verifier=" + verifier + ",";
                header += "oauth_token=" + tkey;
                //header += "realm=\"" + netsuiteAccount + "\"";

                Console.Write(header);

                HttpWebRequest request =
               (HttpWebRequest)WebRequest.Create(urlString);

                request.Method = "GET";
                request.Headers.Add(header);
     
                
                
                WebResponse response = request.GetResponse();
                HttpWebResponse httpResponse = (HttpWebResponse)response;



                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    //Console.WriteLine(JsonConvert.DeserializeObject(reader.ReadToEnd()));
                    string aa = reader.ReadToEnd();
                    //dynamic obj = JsonConvert.DeserializeObject(aa);
                    //MessageBox.Show(aa);
                    textBox5.Text = aa; 
                }

            }
            catch (System.Net.WebException ex)
            {
                HttpWebResponse response = ex.Response as HttpWebResponse;
                switch ((int)response.StatusCode)
                {
                    case 401:
                        Console.WriteLine("Unauthorized, please check your credentials");
                        break;
                    case 403:
                        Console.WriteLine("Forbidden, please check your credentials");
                        break;
                    case 404:
                        Console.WriteLine("Invalid Url");
                        break;
                }
                Console.WriteLine("Code: " + (int)response.StatusCode);

                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    Console.WriteLine("Response: " + reader.ReadToEnd());
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }


        /// <summary>
        /// 取得邮箱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                Dictionary<string,string> pair = this.GetTokenFromUser("oauth_token=1553938889395408897-wDIJi7YRgm8bxFtBJq2qKV6z3jMUpl&oauth_token_secret=nfrNP6kM4QZoSgfP2OBZYxU8c7vrM5o9eqJ7F3bfyjk1F&user_id=1553938889395408897&screen_name=XHowardxing"); 

                //Fill These vars with your netsuite information **********************************************************************

                String query = "";
                //query += System.Web.HttpUtility.UrlEncode("screen_name=" + pair["screen_name"] + "&");
                //query += System.Web.HttpUtility.UrlEncode("count=3&");
                //query += System.Web.HttpUtility.UrlEncode("include_email=true");
                String urlString = "https://api.twitter.com/1.1/account/verify_credentials.json" + query;              //RESTlet uri
                String ckey = "fMvyi3CkipvRI9vnkpDaAwYRY";                   //Consumer Key
                String csecret = "8WjG5EzzYfTRinAy0RY10EG2V5CKxScTpRzOrhlY724WhbiPBz";                //Consumer Secret
                String tkey =  pair["oauth_token"];                 //Token ID
                String tsecret = pair["oauth_token_secret"];      //Token Secret
                //String netsuiteAccount = "";        //Netsuite Account to connect to (i.e 5624525_SB1)
                
                
                //**********************************************************************************************************************

                Uri url = new Uri(urlString);
                OAuthBase req = new OAuthBase();
                String timestamp = req.GenerateTimeStamp();
                String nonce = req.GenerateNonce();
                String norm = "";
                String norm1 = "";
                String signature = req.GenerateSignature(url, ckey, csecret, tkey, tsecret, "", "GET", timestamp, nonce, out norm, out norm1);


                String header = "Authorization: OAuth ";
                header += "oauth_consumer_key=" + ckey + ",";
                header += "oauth_nonce=" + nonce + ",";
                header += "oauth_signature_method=HMAC-SHA1,";
                header += "oauth_timestamp=" + timestamp + ",";
                header += "oauth_version=1.0,";
                header += "oauth_signature=" + signature + ",";
                header += "oauth_token=" + tkey;

                

                //header += "realm=\"" + netsuiteAccount + "\"";

                Console.Write(header);

                HttpWebRequest request =
               (HttpWebRequest)WebRequest.Create(urlString + query);

                request.Method = "GET";
                request.Headers.Add(header);

                WebResponse response = request.GetResponse();
                HttpWebResponse httpResponse = (HttpWebResponse)response;

                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    //Console.WriteLine(JsonConvert.DeserializeObject(reader.ReadToEnd()));
                    string aa = reader.ReadToEnd();
                    //dynamic obj = JsonConvert.DeserializeObject(aa);
                    //MessageBox.Show(aa);
                    textBox5.Text = aa;
                }

            }
            catch (System.Net.WebException ex)
            {
                HttpWebResponse response = ex.Response as HttpWebResponse;
                switch ((int)response.StatusCode)
                {
                    case 401:
                        Console.WriteLine("Unauthorized, please check your credentials");
                        break;
                    case 403:
                        Console.WriteLine("Forbidden, please check your credentials");
                        break;
                    case 404:
                        Console.WriteLine("Invalid Url");
                        break;
                }
                Console.WriteLine("Code: " + (int)response.StatusCode);

                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    Console.WriteLine("Response: " + reader.ReadToEnd());
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }


        /// <summary>
        /// oauth2 获取用户信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void  button4_Click(object sender, EventArgs e)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var twitter = new Twitter.Twitter();
            //twitter.OAuthConsumerKey = "fMvyi3CkipvRI9vnkpDaAwYRY";
            //twitter.OAuthConsumerSecret = "8WjG5EzzYfTRinAy0RY10EG2V5CKxScTpRzOrhlY724WhbiPBz";
            twitter.OAuthConsumerBearer = "AAAAAAAAAAAAAAAAAAAAAITUfQEAAAAAQ3ke1xcRnSCI9T0jNGYf%2FnsBkcg%3D4zRk1vOZ4Lla7NLi10Cb90765BpX1reVAA5KPbhYJqoV7SpQmy";
            var dic = GetTokenFromUser(textBox5.Text);
            string userId = dic["user_id"];
            stopWatch.Stop();
            Console.WriteLine("start1:" + stopWatch.ElapsedMilliseconds);
            stopWatch.Start();
            string t =  twitter.GetTwitts(userId);
            stopWatch.Stop();
            Console.WriteLine("start2:" + stopWatch.ElapsedMilliseconds);
            textBox6.Text = t;
            
        }
       


        /// <summary>
        /// 取得验证
        /// </summary>
        /// <param name="link"></param>
        /// <returns></returns>
        private string[] GetTokenFromLink(string link) {

            if (string.IsNullOrWhiteSpace(link))
            {
                return null;
            }
            if (!link.Contains("?"))
            {
                return null;
            }
            int index = link.IndexOf("?");
            if (link.Length < index + 1)
            {
                return null;
            }

            string li = link.Substring(index+1);
            string[] arry = li.Split('&');
            if (arry.Length < 2)
            {
                return null;
            }
            arry[0] = arry[0].Replace("oauth_token=", "");
            arry[1] = arry[1].Replace("oauth_verifier=","");

            return arry;
        }


        /// <summary>
        /// 取得用户访问token
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private Dictionary<string,string> GetTokenFromUser(string param) 
        {
            
            if (string.IsNullOrWhiteSpace(param))
            {
                return null;
            }
            
            string[] array = param.Split('&');
            //string[] ret = new string[array.Length];
            
            Dictionary<string,string> ret = new Dictionary<string,string>();
            foreach(var item in array)
            {
                var str = item.Substring(item.IndexOf("=") + 1);
                var str2 = item.Substring(0, item.IndexOf("="));
                ret.Add(str2,str);
            }

            return ret;
        }


        
    
    }


 
}
