﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Twitter
{
    public class Twitter
    {
        public string OAuthConsumerSecret { get; set; }        
        public string OAuthConsumerKey { get; set; }
        public string OAuthConsumerBearer { get; set; }

        public  string GetTwitts(string userId)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //string accessToken = await GetAccessToken();
            var requestUserTimeline = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.twitter.com/2/users?ids={0}&user.fields=profile_image_url", userId));
            requestUserTimeline.Headers.Add("Authorization", "Bearer " + OAuthConsumerBearer);
            var httpClient = new HttpClient();
            HttpResponseMessage responseUserTimeLine = httpClient.SendAsync(requestUserTimeline).Result;
            //var serializer = new JavaScriptSerializer();
            //dynamic json = serializer.Deserialize<object>(await responseUserTimeLine.Content.ReadAsStringAsync());
            //var enumerableTwitts = (json as IEnumerable<dynamic>);

            //if (enumerableTwitts == null)
            //{
            //    return null;
            //}
            //return enumerableTwitts.Select(t => (string)(t["text"].ToString()));  
            string str = responseUserTimeLine.Content.ReadAsStringAsync().Result;
            return str;
        }

        public async Task<string> GetAccessToken()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var httpClient = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, "https://api.twitter.com/oauth2/token");
            var customerInfo = Convert.ToBase64String(new UTF8Encoding().GetBytes(OAuthConsumerKey + ":" + OAuthConsumerSecret));
            request.Headers.Add("Authorization", "Basic " + customerInfo);
            request.Content = new StringContent("grant_type=client_credentials", Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpResponseMessage response = await httpClient.SendAsync(request);

            string json = await response.Content.ReadAsStringAsync();
            var serializer = new JavaScriptSerializer();
            dynamic item = serializer.Deserialize<object>(json);
            return item["access_token"];
        }
    }
}