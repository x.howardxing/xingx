﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tools.Common;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";

            //string facebookToken = "EABRcyp8diDQBACJEX4G5NEhZA2e3tWYjnmckr3XZCYZAZBww9HrTmgMVQ371PqNZCZBlOTilL9cWSi3yvtmWSYlg1w59Isl8CEznFQcq59RNoUGISmxigkhIIHx4799ND6z5XqAqGp3fdL3Gn3QCRooBSOWWjaCzj2lePvSVFTbJ8aiqLjJum6N3gEl9a8vGEiwKXlIjpXZCQZDZD";
            string facebookToken = textBox5.Text.Trim2();
            HttpHelper helper = new HttpHelper(System.Text.UTF8Encoding.UTF8);
            Tools.Common.HttpHelper.HttpResult result = helper.Get(string.Format("https://graph.facebook.com/me?fields=id,name,email,picture&access_token={0}", facebookToken));

            Model.Facebook facebook = new Model.Facebook();

            if (result.StatusCode == 200)
            {
                facebook = Tools.Common.JsonHelper.Deserialize<Model.Facebook>(result.ResultHtml);

                if (facebook.id == "" || facebook.id == "0")
                {
                    MessageBox.Show(result.ResultHtml);
                }
                else
                {
                    textBox1.Text = facebook.id;
                    textBox2.Text = facebook.name;
                    textBox3.Text = facebook.picture.data.url;
                    textBox4.Text = facebook.email;
                    MessageBox.Show("success");
                }
            }
            else
            {
                MessageBox.Show("statusCode=" + result.StatusCode);
            }
        }



        

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            string facebookToken = textBox5.Text.Trim2();
            Facebook.FacebookClient client = new Facebook.FacebookClient(facebookToken);
            
            try
            {
                var obj = client.Get<Model.Facebook>(string.Format("me?fields=id,name,email,picture"));
                textBox1.Text = obj.id;
                textBox2.Text = obj.name;
                textBox3.Text = obj.picture.data.url;
                textBox4.Text = obj.email;
                MessageBox.Show("success");
            }
            catch (Facebook.FacebookOAuthException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Facebook.FacebookApiException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }
    }
}
